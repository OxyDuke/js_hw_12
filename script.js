const buttons = document.querySelectorAll(".btn");
const codes = ["Enter", "S", "E", "O", "N", "L", "Z"];
let activeButton = null;

document.addEventListener("keydown", (event) => {
    const key = event.key;
    if (codes.includes(key)) {
        if (activeButton) {
            activeButton.style.backgroundColor = "#000000";
        }
        const button = buttons[codes.indexOf(key)];
        button.style.backgroundColor = "#0000FF";
        activeButton = button;
    }
});